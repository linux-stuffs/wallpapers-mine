# Mine Wallpapers

Mine wallpapers for your desktop.

![wp-01-simple](img/wp-01.jpg)
[[ source ](gimp/wp-01-simple.xcf)
[| black ](1920/wp-01-black.jpg)
[| grey ](1920/wp-01-grey.jpg)
[| blue ](1920/wp-01-blue.jpg)
[| green ](1920/wp-01-green.jpg)
[| red ](1920/wp-01-red.jpg)]

![wp-02-lines](img/wp-02.jpg)
[[ source ](gimp/wp-02-lines.xcf)
[| black ](1920/wp-02-black.jpg)
[| grey ](1920/wp-02-grey.jpg)
[| blue ](1920/wp-02-blue.jpg)
[| green ](1920/wp-02-green.jpg)
[| red ](1920/wp-02-red.jpg)]

![wp-03-abstract](img/wp-03.jpg)
[[ source ](gimp/wp-03-abstract.xcf)
[| black ](1920/wp-03-black.jpg)
[| grey ](1920/wp-03-grey.jpg)
[| blue ](1920/wp-03-blue.jpg)
[| green ](1920/wp-03-green.jpg)
[| red ](1920/wp-03-red.jpg)]

![wp-04-honeycomb](img/wp-04.jpg)
[[ source ](gimp/wp-04-honeycomb.xcf)
[| black ](1920/wp-04-black.jpg)
[| grey ](1920/wp-04-grey.jpg)
[| blue ](1920/wp-04-blue.jpg)
[| green ](1920/wp-04-green.jpg)
[| red ](1920/wp-04-red.jpg)]
